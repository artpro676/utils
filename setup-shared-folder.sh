VERSION='4.2.30'
SHARED_FOLDER='Projects'

mkdir ~/iso
cd ~/iso

wget "http://download.virtualbox.org/virtualbox/${VERSION}/VBoxGuestAdditions_${VERSION}.iso"
sudo mkdir /media/iso
sudo umount /media/iso
sudo mount -o loop -t iso9660 ~/iso/VBoxGuestAdditions_${VERSION}.iso /media/iso

sudo cp -r /media/iso/* ~/iso

cd ~/iso

sudo sh ./VBoxLinuxAdditions.run

sudo adduser $USER vboxsf
sudo mount -t vboxsf ${SHARED_FOLDER} ~/${SHARED_FOLDER}

sudo umount /media/iso
sudo rm -rf ~/iso
sudo rm -rf /media/iso

sudo apt update
sudo apt install -y dos2unix